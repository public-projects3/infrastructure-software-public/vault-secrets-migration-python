import unittest
import main

class mainTests(unittest.TestCase):
    """
    Test Vault Path
    """

    def test_create_path_string(self):
        path = main.create_path('myapp', 'db/creds')
        self.assertIs(type(path), str)

    def test_create_path(self):
        path = main.create_path('myapp', 'db/creds')
        self.assertEqual(path, 'myapp/db/creds', 'Path value failed')

if __name__ == '__main__':
    unittest.main()