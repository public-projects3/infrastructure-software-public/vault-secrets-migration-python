# Overview

This is a python script to migrate secrets between AWS Secrets Manager and HashiCorp Vault.

## Run Instructions

```bash
docker run -it \
    -e VAULT_TOKEN=<your_vault_token> \
    -e VAULT_ADDRESS=<your_vault_server_address> \
    -e VAULT_NAMESPACE=<your_vault_namespace> \
    -e AWS_ACCESS_KEY_ID=AKIA2JGEIFHOITGOE25K \
    -e AWS_SECRET_ACCESS_KEY=p1TS2m6G3wcFk+4eJeMpRk18b5bw3EH35YQAYf+W \
    -v <path_on_your_computer_to_input.csv_file>:/home/appuser/app/input.csv \
    samgabrail/secretsmigration:latest
```
Example:
```bash
docker run -it \
    -e VAULT_TOKEN=s.r57K1KG90OUiznpb6CHXJdjm \
    -e VAULT_ADDRESS=https://127.0.0.1:8200/ \
    -e VAULT_NAMESPACE=admin \
    -e AWS_ACCESS_KEY_ID=BEIA2JG24FHOI453ABCK \
    -e AWS_SECRET_ACCESS_KEY=p1WECm6G3wcFk+abc2e14z18b5wev1235YQAYf+W \
    -v /home/sam/vault-secrets-migration-python/input.csv:/app/input.csv \
    samgabrail/secretsmigration:latest
```
## AWS Documentation

This is the Python [SDK documentation for AWS Secrets Manager.](https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/secretsmanager.html#SecretsManager.Client.get_secret_value)

## Vault HVAC Documentation

This is the Python library to work with [Vault called hvac.](https://hvac.readthedocs.io/en/stable/usage/secrets_engines/kv_v1.html#create-or-update-a-secret)
