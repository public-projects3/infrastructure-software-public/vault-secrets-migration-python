import json
import hvac
import boto3
from botocore.exceptions import ClientError
import os
import csv

def get_secret(secret_name: str, region_name: str) -> dict:
    # Create a Secrets Manager client
    session = boto3.session.Session()
    client = session.client(
        service_name='secretsmanager',
        region_name=region_name
    )

    try:
        get_secret_value_response = client.get_secret_value(
            SecretId=secret_name
        )
    except ClientError as e:
        # For a list of exceptions thrown, see
        # https://docs.aws.amazon.com/secretsmanager/latest/apireference/API_GetSecretValue.html
        raise e

    # Decrypts secret using the associated KMS key.
    secret = get_secret_value_response['SecretString']
    try:
        secret = json.loads(secret)
        return secret 
    except json.decoder.JSONDecodeError as e:
        print('[ERROR] Make sure that the AWS secret is a Key/Value pair and not plain text', e)
        return False
    

def create_path(secret_path: str, secret_name: str) -> str:
    return secret_path + '/' + secret_name

def store_secret(secret_value: dict, secret_name: str, secret_path: str, secret_mountpoint: str):
    client = hvac.Client(url=os.environ['VAULT_ADDRESS'], token=os.environ['VAULT_TOKEN'], namespace=os.environ['VAULT_NAMESPACE'])

    client.secrets.kv.v2.create_or_update_secret(
        mount_point=secret_mountpoint,
        path=create_path(secret_path, secret_name),
        secret=secret_value,
    )

def main():
    with open('input.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                print(f'[INFO] Column names are {", ".join(row)}')
                line_count += 1
            else:
                print(f'[INFO] Working on secret name: {row[1]}')
                secret = get_secret(row[1], row[0])
                if secret:
                    store_secret(secret, row[1], row[3], row[2])
                print(f'Processed {line_count} secrets.')
                line_count += 1
        

if __name__ == "__main__":
    main()