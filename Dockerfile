# docker build -t samgabrail/secretsmigration:latest .
# docker push samgabrail/secretsmigration:latest
FROM python:3.8.15-slim-buster
LABEL maintainer="Sam Gabrail"
RUN useradd -ms /bin/bash appuser
WORKDIR /home/appuser/app
COPY . .
RUN apt-get update && pip install -r requirements.txt
USER appuser
CMD [ "python", "-u", "main.py" ]